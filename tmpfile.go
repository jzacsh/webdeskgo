package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

// TODO(zacsh) replace this with a slightly altered copy/paste of the golang
// implementation of TempFile (but with the obvious bug fixed), then remove all
// this hackery done here
func (d *desk) mktemp(prefix, suffix string) (*os.File, error) {
	tmpFile, e := ioutil.TempFile(d.Tmpdir, prefix)
	if e != nil {
		return nil, fmt.Errorf("starting generated file:\n%s\n", e)
	}

	if strings.HasPrefix(suffix, ".") {
		suffix = strings.TrimPrefix(suffix, ".")
	}

	tmpPath := fmt.Sprintf("%s.%s", tmpFile.Name(), suffix)
	f, e := os.Create(tmpPath)
	if e != nil {
		return nil, fmt.Errorf("naming generated file:\n%s\n", e)
	}

	tmpFile.Close()
	if e := os.Remove(tmpFile.Name()); e != nil {
		return nil, e
	}

	return f, nil
}
