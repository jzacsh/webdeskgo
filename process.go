package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"syscall"
	"time"
)

// data around which a SINGLE log will be output per request
type oneRequestLog struct {
	FailureMsg string // empty indicates we succeeded
	Method     string // http.Request.Method value
	RemoteAddr string // http.Request's RemoteAddr field
	Url        string // url.URL's String() output, from http.Request.URL

	// TODO(zacsh) revsit; Below is hardcoded/specific to a type of desk processor
	// (eg: specific to `pdfbook` interface; but might need to be fixed in future)
	FileName       string        // per the multipart.FileHeader of http.Request's FormFile() resposne
	InputFileSize  int64         // length in bytes, of PDF file that we recieved; may not be whole PDF
	OutputFileSize int64         // length in bytes, of PDF file that we returned; also may not be whole PDF
	ExecedFor      time.Duration // how long shell out took
}

func (orl *oneRequestLog) failWith(w http.ResponseWriter, e error, msg string) {
	const maxLen int = 1000

	var err string
	if len(e.Error()) > maxLen {
		err = fmt.Sprintf(
			"%s [snipped, was %d chars longer]",
			e.Error()[:maxLen], len(e.Error())-maxLen)
	} else {
		err = e.Error()
	}
	orl.FailureMsg = fmt.Sprintf(fmt.Sprintf("fatal: %s", msg), err)

	w.WriteHeader(http.StatusInternalServerError)
	fmt.Fprintf(w, orl.FailureMsg)
}

func (orl *oneRequestLog) String() string {
	baseLog := fmt.Sprintf("%s to %s from %s",
		orl.Method,
		orl.Url,
		orl.RemoteAddr)
	if orl.Method != "POST" {
		if len(orl.FailureMsg) > 0 {
			return fmt.Sprintf("%s; %s", baseLog, orl.FailureMsg)
		}
		return baseLog
	}

	// TODO(zacsh) revisit: below is hardcoded to PDFbook handling

	pdfProcessRequest := fmt.Sprintf("%s; PDF='%s' >= %d bytes",
		baseLog,
		orl.FileName,
		orl.InputFileSize)

	if len(orl.FailureMsg) > 0 {
		return fmt.Sprintf("%s; FAIL: %s", pdfProcessRequest, orl.FailureMsg)
	}

	return fmt.Sprintf("%s; SUCCESS in %s, producing %d bytes",
		pdfProcessRequest,
		orl.ExecedFor,
		orl.OutputFileSize)
}

func buildStarterRequestLog(r *http.Request) oneRequestLog {
	return oneRequestLog{
		Url:        r.URL.String(),
		RemoteAddr: r.RemoteAddr,
		Method:     r.Method,
	}
}

func (orl *oneRequestLog) logHandled() {
	log.Printf("handled: %s\n", orl.String())
}

func (d *desk) process(w http.ResponseWriter, r *http.Request) {
	orlg := buildStarterRequestLog(r)
	defer orlg.logHandled()

	if r.Method == "GET" {
		d.Template.Execute(w, nil)
		return
	}

	if r.Method != "POST" {
		orlg.failWith(w, fmt.Errorf("got unexpected HTTP method"), "not implemented: %s")
		return
	}

	mPartFile, uploadedHeader, e := r.FormFile("usrupld")
	if e != nil {
		orlg.failWith(w, e, "problem reading upload: %s")
		return
	}
	defer mPartFile.Close()

	orlg.FileName = uploadedHeader.Filename
	usrExt := filepath.Ext(uploadedHeader.Filename)

	upld, e := d.mktemp("webfile-usrupload_", usrExt)
	if e != nil {
		orlg.failWith(w, e, "starting temp file to rec'v upload: %s")
		return
	}
	defer upld.Close()

	reqPDFSize, e := io.Copy(upld, mPartFile)
	orlg.InputFileSize = reqPDFSize
	if e != nil {
		orlg.failWith(w, e, "saving local copy of upload: %s")
		return
	}

	gen, e := d.mktemp("webfile-generated_", usrExt)
	if e != nil {
		orlg.failWith(w, e, "starting generated file: %s")
		return
	}
	defer gen.Close()

	// TODO(zacsh) here & below: all hard-coded handling - perhaps worth breaking
	// out into "pdfbook.go" file, and calling in a switch/case selection when
	// another type of process is desired (eg: indicated by web form's radio
	// selection

	cmd := exec.Command("pdfbook", upld.Name(), "--quiet", "--outfile", gen.Name())
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	now := time.Now()
	e = cmd.Run()
	orlg.ExecedFor = time.Since(now)

	if e != nil {
		orlg.failWith(w, e, "creating pdfbook: %s")
		return
	}

	outName := fmt.Sprintf(
		"%s-book%s",
		strings.TrimSuffix(uploadedHeader.Filename, usrExt),
		usrExt)

	w.Header().Set(
		"Content-Disposition",
		fmt.Sprintf("attachment; filename=%s", outName))
	w.Header().Set("Content-Type", "application/pdf")

	respPDFSize, e := io.Copy(w, gen)
	orlg.OutputFileSize = respPDFSize

	if e != nil {
		orlg.failWith(w, e, "sending back generated file: %s")
		return
	}

}
