package main

import (
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"regexp"
)

type desk struct {
	Port     string
	Tmpdir   string
	Template *template.Template
}

func (d *desk) cleanup(exitWith int) {
	log.Printf("cleaning up tempdir:\n\t%s\n", d.Tmpdir)
	if e := os.RemoveAll(d.Tmpdir); e != nil {
		log.Printf("cleaning failed: %v\n", e)
	} else {
		log.Printf("cleanup done\n")
	}

	if exitWith < 0 {
		return
	}

	os.Exit(exitWith)
}

func buildProcessingDesk() *desk {
	d := &desk{Port: ":9090"}
	portType := "default"
	if len(os.Args) > 1 {
		portType = "custom"
		d.Port = os.Args[1]

		portRegexp := regexp.MustCompile(`^:\d\d\d?\d?$`)
		if !portRegexp.MatchString(d.Port) {
			log.Fatalf(
				"bad port, must be colon then 2-4 numbers, eg: ':1234'; got '%s'\n",
				d.Port)
		}
	}
	log.Printf("serving on %s port, %s\n", portType, d.Port)

	dir, e := ioutil.TempDir("" /*use default*/, "webdesk-processor_userfiles_")
	if e != nil {
		log.Fatalf("failed starting tempdir: %v", e)
	}
	d.Tmpdir = dir
	log.Printf("started tempfiles bucket: %s\n", d.Tmpdir)

	d.Template = submitTemplate()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		s := <-c
		log.Printf("caught signal: %v", s)
		d.cleanup(0)
	}()
	return d
}

func main() {
	d := buildProcessingDesk()

	http.HandleFunc("/", d.process) // setting router rule
	if err := http.ListenAndServe(d.Port, nil); err != nil {
		d.cleanup(-1 /*exitWith; do not exit*/)
		log.Fatal("ListenAndServe: ", err)
	}
}
