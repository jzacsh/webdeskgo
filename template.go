package main

import "html/template"

func submitTemplate() *template.Template {
	return template.Must(template.New("submitpage").Parse(`
<html>
  <head>
    <title>Process</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
      body {
        margin: 0 auto;
        text-align: center;
      }
      form > label {
        display: block;
        padding: 1em 0;
        padding: 1em auto;
      }
      span.instruction {
        font-weight: bold;
        font-variant: small-caps;
        display: block;
      }
      span.instruction:after {
        content: ":";
      }
    </style>
  </head>
  <body>
    <h1>BookIFY a PDF</h1>
    <form enctype="multipart/form-data" action="/" method="post">
      <label for="usrupld">
        <p><span class="instruction">Step 1</span> Select a PDF</p>
        <input required type="file" name="usrupld" accept="application/pdf">
      </label>

      <label for="submit">
        <span class="instruction">Step 2</span>
        <input type="submit" value="BookIFY it">
      </label>
    </form>

    <footer>
      <a href="https://gist.github.com/jzacsh/842c211e2e524a70c6a8c7c8788ff253">Source
      code</a> for this page and its server is freely available.
    </footer>
  </body>
</html>
`))
}
